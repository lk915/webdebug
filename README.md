### 郑重声明：请同意并遵守互联网法律法规，本工具仅供学习调试使用，切勿用于非法用途，不然后果自负，与本作者无关。在压测调试的时候使用此功能时您的IP，压测记录将记录到远程，本工具已多次提醒和建议只能用于学习调试，不得用于非法目的，如果你的行为触犯相关法律，与本作者无关，本作者将配合警方提供你的犯罪记录，概不负责。

一款免费的同时支持windows与Linux的web调试工具，支持Mysql日志监控，网页与api无刷新性能调试，支持网页与api压力测试，支持文件管理，字符串全局搜索替换，支持正则随心匹配，支持代码热编译功能与页面定位固定刷新功能再也不用手动刷新和滚动鼠标了，还有贴心的百宝箱功能，非常傻瓜式的一款工具，使用时请遵守互联网法律法规<br/>
###  【注意事项】 ###
0：工具使用教程地址：[https://space.bilibili.com/448720070?spm_id_from=333.788.b_765f7570696e666f.2](https://space.bilibili.com/448720070?spm_id_from=333.788.b_765f7570696e666f.2)<br/>
1：小工具需要本机支持安装了mysql，建议屏幕分辨率在1920*1080px或以上<br/>
2：mysql 工具默认信息是：账号 root 密码 root 主机是：0.0.0.0（127.0.0.1）如果一样请直接回车， 如果您本机不是重新输入你本机信息即可。<br/>3：mysql日志默认是30 需要修改输入数字即可<br/>
4：使用时请遵守互联网法律法规，压力测试不要去压别人站点哦！<br/>
5：小工具可能很粗糙，还在改进需要有兴趣的大神可以多提提意见。<br/>
6：支持在线编辑与实时预览与页面定位固定刷新功能再也不用手动刷新和滚动鼠标了<br/>
7：强大的字符串与目录全局搜索功能，搜索建议到对应文件夹搜索快，不要直接在盘符下全盘搜索，比较慢<br/>
8：还有百宝箱功能<br/>
9：支持正则表达式即实随心匹配调试与替换<br/>
10: 支持多级目录创建，与多文件同时增删改查,支持图片预览<br/>
11: APi与压力测试支持通过增加header与cookie **(暂时还不支持多维度的cookie哦)** 参数模拟登录调试与性能测试    <br/>
12: 如果本工具哪里引用或者使用了您的插件或者工具对您的版权有侵犯，请立即联系本作者，我将立即去除<br/>


![输入图片说明](https://images.gitee.com/uploads/images/2020/0608/001508_abbdf335_5332980.jpeg "0.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0508/142020_aafe2b27_5332980.png "0.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0608/002614_42878e85_5332980.jpeg "1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0607/103312_1f0dda7b_5332980.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/230431_a0af3bd3_5332980.jpeg "3.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0424/213055_fffcc25f_5332980.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0424/213402_6bb80166_5332980.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0329/111135_f7f09f58_5332980.jpeg "1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0401/070656_c0d607d5_5332980.jpeg "3.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0227/175630_fd7800ee_5332980.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0227/171846_29cd00ad_5332980.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0227/174411_f7a91086_5332980.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/131409_fd1de3dc_5332980.png "预览.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0229/105355_e7fd0d62_5332980.jpeg "0.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0227/172108_f2df4f1a_5332980.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0228/095433_91744762_5332980.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0227/172236_2a74fd12_5332980.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/175838_217a5928_5332980.jpeg "1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/175847_d78290b2_5332980.jpeg "2.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/175855_53a3db19_5332980.jpeg "3.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/175905_29382c04_5332980.jpeg "4.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/175916_d7049d01_5332980.jpeg "5.jpg")
