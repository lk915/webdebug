<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{.title}}</title>
    <link rel="shortcut icon" type="image/x-icon" href="/static/img/favicon.ico" />
    <link rel="stylesheet" href="/static/css/main.css?t=lzy2020558s59d499">
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <script src="/static/layui/layui.all.js"></script>
    <script src="/static/js/jquery-1.10.2.js"></script>
    <script src="/static/js/init.js?t=lzy2edds05d20hs848788520"></script>
    <script src="/static/ace/ace.js?t=lzy"></script>
    <script src="/static/ace/ext-beautify.js?t=lzys"></script>
    <script src="/static/ace/ext-language_tools.js?t=lz2ys"></script>
    <script src="/static/ace/theme-monokai.js?t=lzy2s"></script>
    <script src="/static/js/Tdrag.min.js"></script>
    <script src="/static/js/echarts.min.js"></script>
    <script src="/static/js/main.js?t=lzy9911299999"></script>
    <script src="/static/js/websocket.js?t=lzy9188888"></script>
    <script src="/static/js/com.js?tt=lzy20ssss221d2d0"></script>
</head>
<body>
<iframe src="/static/instructions.html" frameborder="0"  id="iframe"></iframe>
<iframe src="" frameborder="0"  id="show" style="display: none;"></iframe>
<div class="apiBody">
    <pre></pre>
</div>
<div class="performance">
    <div class="performance-haoshi"></div>
    <div class="performance-map" id="performance-map"></div>
</div> 
<div class="setHetBox">
    <p class="iframeWidthSetP">
            锁定页面展示宽度：<input type="range" name="iframeWidthSet" min="0" max="1920" id="iframeWidthSet" step="1" value="0" onchange="iframeWidthSet()"/>
        </p>
    <p  class="iframeHeightSetP">
            锁定页面展示高度：<input type="range" name="points" min="0" max="10000" id="iframeHeightSet" step="1" value="0" onchange="setIframeHeight()"/>
    </p>
    <div class="serachP">
        <p class="addDir">
            <span onclick="openEdit()" id="openEdit">打开编辑器</span> | 
            <input id="addDirName" type="text"/>
            <input name="addDir" type="radio" value="file" checked="checked"/> 文件
            <input name="addDir" type="radio" value="dir"  /> 目录      
            <button class="addDirbutton" onclick="addDir()">新建</button> 
        </p>
        当前路径：<span class="nowPath"></span>
        <ul class="dirLists"></ul>
    </div> 
    <span class="close" onclick="closeSetHetBox(this)"></span> 
</div>
<div class="box">
        选择模式：
        <select name="mode" id="debugModel" class="debugModel"  onchange="changeModel()">
            <option value="NORMAL">普通网页</option>
            <option value="GET">Api.get</option>
            <option value="POST">Api.post</option>
            <option value="YALI-GET">压力测试.网页</option>
            <option value="YALI-POST">压测Api.post</option>
            <option value="REGXP">正则表达式</option>
            <option value="CHEST">百宝箱</option>
        </select>
        <input type="hidden" id="isOpen" value="no">
        <input type="text" id="url" placeholder="大神，是时候填上你要debug的操作了！" onkeyup="url()" onblur="updateModelsUrlValues()">
        <span class="Interval YALISHOW">间隔：<input type="text" id="Second" value="5"> 秒</span>
        <button id="DeBug" onclick="DeBug()">开始</button>
        <span class='YALISHOW'><button id="stop" onclick="stop()">停止</button></span>
        <button id="OpenMysql" onclick="OpenMysql()" data-status="yes">打开mysql监控</button>
        <button id="hiden" onclick="hidenDebug()">最小化</button>
        <div class="params">
            <div class="bingfa-params">
                并发数：<input type="text" class="C" value="10">
                链接数：<input type="text" class="N" value="1000">
                是否自动继续压测：<input type="checkbox" class="isAutoYaCe" value="yes">
                <br/>
                <br/>
                <div class="AgreementBox">
                    <input type="checkbox" name="agreement" class="agreement" value="ok"> 
                    同意与注意：
                </div>
                <p>1:请遵守互联网法律法规。</p>
                <p>2:并发数不能大于链接数。</p>
                <p>3:Mysql压测期间后台会关闭监控。</p>
                <p>4:压力测试时请不要开启多个调试页面。</p>
                <p>5:并发数与链接数最好成整数倍，最好10倍以上比较好。</p>
                <p>6:请debug自己的站点或者api，请勿恶意去测试别人站点。</p>
                <p>7:本工具仅供学习调试使用，请勿使用该功能用于非法用途，如果触犯相关法律概不负责，且发现将收回使用权限。</p>
                <p>8:你勾选同意将视为同意以上条款，使用此功能时您的IP，压测记录将记录到远程，本工具已多次提醒和建议只能用于学习调试，不得用于非法目的，如果你的行为触犯相关法律，与本作者无关，本作者将配合警方提供你的犯罪记录，概不负责。</p>
            </div>

            <button  class="addParam"  onclick="ClearSql()">清空SQL日志</button>
            <button  class="addParam"  onclick="GetSql()">提取当前SQL语句</button>
            <button  class="addParam"  onclick="translation()">有道词典翻译</button>
            <button  class="addParam"  onclick="jsonFormat()">json格式化</button>
            <button  class="addParam"  onclick="addParam()">添加参数</button>
           
            <span class="lockPageDisplaySpan"> 设置与代码热编译：<input type="checkbox" name="lockPageDisplay" id="lockPageDisplay"    onclick="lockPageDisplay()"></span>
            <em id="ShowMsg"></em>
            <div class="params-list">
                <form action="#" id="params-form">
                        <p><span class="params-span params-span-active" onclick="setParamsChecked(this)"><input name="keyValueType" type="radio" value="normal" checked  />普通参数 </span><span class="params-span" onclick="setParamsChecked(this)"><input name="keyValueType" type="radio" value="header"   /> Header参数</span><span class="params-span" onclick="setParamsChecked(this)"><input name="keyValueType" type="radio" value="cookie"   />Cookie参数  </span>&nbsp;&nbsp;&nbsp;&nbsp;Key：<input type='text' class='key' class="keyValue"/>Value：<input type='text' class='value' class="keyValue" /><button onclick=delParam(this)>删除</button> </p>
                </form>
            </div>
        </div>
        <div class="regText">
                <p>
                    全局搜索：<input type="checkbox" name="regGlobal" class="regGlobal"  id="regGlobal" value="ok" checked>  &nbsp;&nbsp;&nbsp;&nbsp;
                    区分大小写：<input type="checkbox" name="regCase" class="regCase"  id="regCase" value="ok" checked>  &nbsp;&nbsp;&nbsp;&nbsp;
                    页面抓取：<input type="checkbox" name="pageGet" class="pageGet"  id="pageGet"  onclick="pageGet()">  &nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="pageGetBox">
                        网址：<input type="text" id="pageGetInput" placeholder="请遵守robots.txt协议和互联网法律法规哦！">
                        <button onclick="GetPageContent()" id="GetPageContent">走你</button>
                    </span>
                    <span class='replaceSpan'></span>
                </p>
                <p>内容：<textarea name="regStr" id="regStr" class="regStr" onkeyup="regXp()" onblur="regXp()"></textarea></p>
       </div>
       <div class="chestBox">
           <p>
                <span class="params-span " onclick="setChestParamsChecked(this)"><input name="chest" type="radio" value="STRSEARCH" onclick="changeChest(0)"  /> 文件系统管理(字符串搜索替换)</span>
                <span class="params-span " onclick="setChestParamsChecked(this)" ><input name="chest" type="radio" value="DIRSEARCH" onclick="changeChest(0)"  /> 目录或文件搜索</span>
                <span class="params-span " onclick="setChestParamsChecked(this)" ><input name="chest" type="radio" value="IPTOCITY" onclick="changeChest(0)"/> IP查询  </span>
                <span class="params-span " onclick="setChestParamsChecked(this)" ><input name="chest" type="radio" value="TIMETODATE" onclick="changeChest(0)"/> 时间戳转日期 </span>
           </p>
           <div class="serachP">
               当前路径：<span class="nowPath"></span>
               <ul class="dirLists"></ul>
           </div>  
       </div>
</div>
<div id="Mysqlbox">

</div>
<div class="codeBox">
    <div class="codeInstructions">
        <p  class="codeBoxWidthSetP">
            修改页面宽度：
            <input type="range" class="codeBoxWidthRange" min="0" max="1920" id="codeBoxWidthRange" step="1" value="960" onchange="codeBoxWidthSet()">
        </p>
        <p>代码编辑会自动保存，如果编辑的文件在网页，api.get，api.post三个模式下的目录里面会实时编译自动刷新</p>
        <p style="display: none;">当前打开文件路径:&nbsp;<span class="fileDir"></span></p>
        <p>字体大小:&nbsp;
            <input type="text" onkeyup="SetFontSize(this)" value="16" class="SetFontSize">
            编程语言/框架修改:
            <select name="SetLang" id="SetLang" onchange="SetLang()"></select>
            <i>(编程语言打开编辑会自动默认初始化匹配，如果有误请修改对应的语言或框架)</i>
        </p>
        <p><span class="fileLists"></span></p>
    </div>
    <div class="codeMain" id="codeMain" onkeyup="Code()">

    </div>
    <span class="close" onclick="closeCodeBox(this)"></span>
</div>
<div href="javascript:;" class="openDebug" onclick="openDebug()">打开调试</div>
<div style="display:none;" class="editorBox"><div id="editor"></div></div>
</body>
</html>